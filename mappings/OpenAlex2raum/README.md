# OpenAlex2raum

This directory contains mappings from OpenAlex concepts to FINDEX Raum concepts. They are derived from [/mappings/raum2OpenAlex/raum2OpenAlex%2Bmetadata.ttl](/mappings/raum2OpenAlex/raum2OpenAlex%2Bmetadata.ttl). In particular, the first version of this mapping set was created with <https://gitlab.com/fid-bau/terminologie/findex-raum/-/raw/95310cd57e8352828a4b1542fe60a2ee29cd5a1c/mappings/raum2OpenAlex/raum2OpenAlex+metadata.ttl>. They are intended to be used in [FID BAUdigital Forschungsatlas](https://forschungsatlas.fid-bau.de/).

The mapping set is available via this PURL: <https://purl.org/fidbaudigital/OpenAlex2raum/fa>.

It contains 320 exact matches (skos:exactMatch). In few cases, a concept is subject or object in several mapping statements.

## Procedure

|#|step|outputs|
|-|-|-|
|1|Import [/mappings/raum2OpenAlex/raum2OpenAlex%2Bmetadata.ttl](/mappings/raum2OpenAlex/raum2OpenAlex%2Bmetadata.ttl) with OpenRefine.<br>Import format: RDF/Turtle, character encoding UTF-8.|--|
|2|process imported file with [/mappings/OpenAlex2raum/procedure.json](/mappings/OpenAlex2raum/procedure.json)|--|
|3|Export project as RDF Turtle.|[/mappings/OpenAlex2raum/OpenRefine_export.ttl](/mappings/OpenAlex2raum/OpenRefine_export.ttl)|
|4|Open [/mappings/OpenAlex2raum/OpenRefine_export.ttl](/mappings/OpenAlex2raum/OpenRefine_export.ttl) with Protege or other ontology editor and save files in Turtle syntax (.ttl) and RDF/XML synatx (.owl).<br>This steps gets rid of redundant occurrences of statements and renders the statements in a more readable format.|undocumented product|
|5|Add versionIRI to each output of step 4.|[/mappings/OpenAlex2raum/OpenAlex2raum.owl](/mappings/OpenAlex2raum/OpenAlex2raum.owl)<br>[/mappings/OpenAlex2raum/OpenAlex2raum.ttl](/mappings/OpenAlex2raum/OpenAlex2raum.ttl)|