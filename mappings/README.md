# Mappings

This directory contains mappings from Findex Raum to other vocabularies.

|Mapping set|description|SSSOM|PURL to latest|
|-|-|-|-|
|[raum2bau](/mappings/raum2bau/)|Mappings to Findex Bau|false|<https://www.purl.org/fidbaudigital/raum2bau>|
|[raum2gnd](/mappings/raum2gnd/)|Mappings to GND (tbd)|tbd|tbd|
|[raum2OpenAlex](/mappings/raum2OpenAlex/)|Mapping to concepts on OpenAlex|true|https://www.purl.org/fidbaudigital/raum2OpenAlex|
|[OpenAlex2raum](/mappings/OpenAlex2raum/)|Mappings between Findex Raum and OpenAlex, derived from [/mappings/raum2OpenAlex/raum2OpenAlex%2Bmetadata.ttl](/mappings/raum2OpenAlex/raum2OpenAlex%2Bmetadata.ttl). Not employing full SSSOM schema. Intended for [FID BAUdigital Forschungsatlas](https://forschungsatlas.fid-bau.de/).|false|<https://purl.org/fidbaudigital/OpenAlex2raum/fa>|
<!-- ||||| -->
<!-- ||||| -->