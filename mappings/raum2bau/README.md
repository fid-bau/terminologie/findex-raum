# raum2bau

## Introduction
FINDEX Bau and FINDEX Raum have quite some overlap in concepts, therefore they were mapped with each other. The mappings from FINDEX Raum to FINDEX Bau can be found here.

## Source
These mappings have been created with a SPARQL CONSTRUCT query from the mapping file [https://gitlab.com/fid-bau/terminologie/findex-bau/-/raw/0.0.1/mappings/bau2raum.owl](https://gitlab.com/fid-bau/terminologie/findex-bau/-/raw/0.0.1/mappings/bau2raum.owl). Please see the limitations on these mappings at <https://gitlab.com/fid-bau/terminologie/findex-bau/-/blob/0.0.1/mappings/README.md>.

## Contributions
If you find any errors or do not agree, we would like to encourage you, to [get in touch with us](https://gitlab.com/fid-bau/terminologie/findex-bau/-/issues).

## Mapping distributions
You find the following mapping files:
|File|comment|PURL to latest|
|-|-|-|
|[/mappings/raum2bau/raum2bau.owl](/mappings/raum2bau/raum2bau.owl)|This file contains mappings from FINDEX Raum to FINDEX Bau, applying `skos:mappingRelation` types. This is based on the master file for mappings between FINDEX Bau and FINDEX Raum at [FINDEX Bau > bau2raum.owl](https://gitlab.com/fid-bau/terminologie/findex-bau/-/raw/0.0.1/mappings/bau2raum.owl).|https://www.purl.org/fidbaudigital/raum2bau|
|[/mappings/raum2bau/raum2bau_exactMatch_DbXRef.owl](/mappings/raum2bau/raum2bau_exactMatch_DbXRef.owl)|This contains mappings from FINDEX Raum to FINDEX Bau, applying the mapping property <http://www.geneontology.org/formats/oboInOwl#hasDbXref>. This file will be used on the TIB Terminology Service. It has been created from [/mappings/raum2bau/raum2bau.owl](/mappings/raum2bau/raum2bau.owl) applying a SPARQL CONSTRUCT query. Only `skos:exactMatches` are present in this file.|none|

## PURLs

- to latest version: <https://www.purl.org/fidbaudigital/raum2bau>
- older versions: cf. <https://gitlab.com/fid-bau/terminologie/findex-raum/-/releases>

## Roadmap

- switch to SSSOM for mapping documentation