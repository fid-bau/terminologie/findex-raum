# FINDEX Raum

## PURL und Versionsinformation

* PURL: <https://terminology.fraunhofer.de/voc/raum>
* Version: 0.0.7
* Version IRI: <https://gitlab.com/fid-bau/terminologie/findex-raum/-/raw/0.0.7/findex-raum.owl>
* [Alle Releases](https://gitlab.com/fid-bau/terminologie/findex-raum/-/releases)

## Lizenz

[![Creative Commons Lizenzvertrag](https://i.creativecommons.org/l/by-sa/4.0/88x31.png)](http://creativecommons.org/licenses/by-sa/4.0/)

Dieses Werk ist lizenziert unter einer <a rel="license" href="https://creativecommons.org/licenses/by-sa/4.0/">Creative Commons Namensnennung - Weitergabe unter gleichen Bedingungen 4.0 International Lizenz.

## Vorschläge und Beteiligung

Bitte beachten Sie hierzu unsere [Hinweise zur Beteiligung](/CONTRIBUTING.md).

## Über den FINDEX Raum

Die SKOS-Version des FINDEX Raum wurde vom Fraunhofer-Informationszentrum Raum und Bau IRB im Kontext des gemeinsamen Verbund-Projekts FID BAUdigital in Kooperation mit der TIB - Technische Informationsbibliothek erstellt. Sie basiert lose auf der 2. unveränderten Auflage des FINDEX Raum, die 1993 durch den IRB-Verlag veröffentlicht wurde.

Die Print-Version der 1. Auflage des FINDEX Raum wurde auf der Grundlage des von der Bundesforschungsanstalt für Landeskunde und Raumordnung (BfLR) 1984 herausgegebenen Thesaurus Raumplanung/Stadtplanung entwickelt.  Dessen Entwurfsfassung war 1983 in die Struktur des Findex Raum umgesetzt worden. Im Anschluss an die Publikation wurde das Begriffsmaterial des Findex Raum in einer Kooperation zwischen der BfLR, dem Deutschen Institut für Urbanistik (Difu), dem Institut für Landes- und Stadtentwicklungsforschung (ILS), dem Österreichischen Institut für Raumplanung (ÖIR), dem Salzburger Institut für Raumforschung (SIR) und dem Informationszentrum Raum und Bau (IRB)abgestimmt.

Der FINDEX Raum wurde in Anlehnung an den FINDEX Bau als facettenartiges Indexierungssystem erstellt. FINDEX Raum wurde zur Indexierung der Informationen der deutschsprachigen Literaturdatenbanken RSWB (= Raumordnung, Städtebau, Wohnungswesen, Bauwesen) und LNA (= Literaturnachweise) und der Forschungsprojektdatenbank FORS (= Raumordnung, Städtebau, Wohnungswesen) verwendet.

FINDEX Raum ist eine Begriffssammlung zum Ordnen und Wiederauffinden von Informationen und orientiert sich hauptsächlich an den Erfordernissen der Dokumentationspraxis. FINDEX Raum erhebt deshalb keine Ansprüche auf Vollständigkeit.

Der FINDEX Raum ist in vier Ebenen gegliedert, wobei die erste Ebene die folgenden achtzehn Hauptbegriffe enthält. Sie werden in den anderen Ebenen weiter untergliedert.

* Bau/Gebäude/Wohnung
* Bevölkerung/Gesellschaft
* Bildung/Kommunikation/Information
* Finanzen
* Freizeit/Sport/Fremdenverkehr
* Gesundheit/Hygiene
* Öffentliche Sicherheit
* Organisationen/Institutionen
* Planung
* Raum und Siedlung
* Raumgrundlage
* Recht
* Staat/Verwaltung
* Technische Infrastruktur
* Umwelt und Landschaftspflege
* Verkehr
* Wirtschaft
* Wissenschaft/Theorie/Methode

Die SKOS-Version versucht die ursprüngliche Struktur des FINDEX Raum so genau wie möglich mit den durch den SKOS-Standard (vgl. <https://www.w3.org/TR/skos-reference/>) bereitgestellten Mitteln abzubilden.

Im Rahmen des FID BAUdigital soll die SKOS-Version des FINDEX Raum als ein Modul neben weiteren Thesaurus-Modulen für den Einsatz in den Informationsportalen des FID BAUdigital eingesetzt werden. Seine inhaltliche Weiterentwicklung ist hierbei nicht geplant. Stattdessen soll ihm ein weiteres Modul zur Seite gestellt werden, das Begriffe des digitalen Planens und Bauens beinhalten wird.

## Zitation

Fraunhofer-Informationszentrum Raum und Bau IRB (Hrsg.): FINDEX Raum als SKOS-Thesaurus. Facettenartiges Indexierungssystem für Raumordnung, Städtebau, Wohnungswesen, 2021. URL: <https://terminology.fraunhofer.de/voc/raum>.

## Referenzen

* Fraunhofer-Informationszentrum Raum und Bau IRB (Hrsg.): FINDEX Raum. Facettenartiges Indexierungssystem für Raumordnung, Städtebau, Wohnungswesen. Eine systematische Begriffssammlung zum Ordnen und Suchen von Informationen. Systematischer und alphabetischer Teil. 2., unveränd. Auflage, Stuttgart: Fraunhofer IRB Verlag, 1993
* Fraunhofer-Informationszentrum Raum und Bau IRB (Hrsg.): Facet-oriented Indexing System for Architecture and Construction Engineering. A systematic Collection of Terms for Classification and Searching of Building Informations. Systematic and Alphabetic Part, 1. Aufl., Stuttgart: Fraunhofer IRB Verlag, 1985. Eintrag im Katalog der Deutschen Nationalbibliothek: <http://d-nb.info/860068544>.
* Bundesforschungsanstalt für Landeskunde und Raumordnung - BfLR (Hrsg.): Thesaurus Raumplanung/Stadtplanung. 3 Bde., Bonn: BfLR, 1984.

## Externe Links

* [Findex Raum auf dem TIB Terminology Service](https://terminology.tib.eu/ts/ontologies/raum)

## Schlagwörter

* [Raumordnung](http://d-nb.info/gnd/4048590-0)
* [Wohnungsbau](http://uri.gbv.de/terminology/bk/56.81)
* [Raumordnung. Städtebau: Allgemeines](http://uri.gbv.de/terminology/bk/74.60)
* [Raumordnung, Stadtplanung, Landschaftsgestaltung](https://d-nb.info/standards/vocab/gnd/gnd-sc#10.7b)
