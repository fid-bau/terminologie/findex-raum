# Contributions FAQ

**What kind of contributions can be made?**

You can report errors or technical issues in Findex Raum.

**How can I report errors and technical issues?**

To report errors and technical issues, you should file an [issue](https://gitlab.com/fid-bau/terminologie/findex-raum/-/issues).

If you leave an issue, make sure to:

* provide a precise description of the problem
* list all or examplary identifiers, if your report or suggestions concern specific concepts
* if possible describe your suggested solution

**What if I want to suggest a new concept or other content changes to Findex Raum?**

At the moment, we do not recommend to suggest conceptual changes to FINDEX Raum. We will still evaluate your suggestions but this may take a little time and the suggestions may be rejected.