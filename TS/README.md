# Erstellen einer TS-Distribution des Findex Raum

Im Ordner TS liegt eine Kopie des Findex Raum, die für die Einbindung des Findex Raum in den Terminology Service der Technischen Informationsbibliothek verwendet wird. Diese Kopie ist inhaltlich identisch mit der SKOS-Version, wurde jedoch durch Statements angereichert.

1. Ersetze [TS\findex-raum.owl](TS\findex-raum.owl) durch [findex-raum.owl](findex-raum.owl).

2. Öffne [TS\findex-raum.owl](TS\findex-raum.owl) mit Protege.

3. Importiere <https://purl.org/fidbaudigital/subjects#P49dfc9fd-8edc-46f8-ac84-f58a207cb9fe>. Mit der Refactor-Option von Protege werden alle Axiome in die Kopie des Findex Raum kopiert. Hierzu wird die Methode "axioms by definition" verwendet.

4. Führe folgenden Query aus:

    ```SPARQL

    PREFIX owl: <http://www.w3.org/2002/07/owl#>
    PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
    PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
    prefix skos: <http://www.w3.org/2004/02/skos/core#>


    CONSTRUCT {
        ?s a owl:Class . 
        ?s <https://purl.org/fidbaudigital/subjects#P49dfc9fd-8edc-46f8-ac84-f58a207cb9fe> ?notation .
        }

    WHERE {
        ?s a skos:Concept .
        ?s skos:notation ?notation .
        FILTER (STRSTARTS(str(?s), "https://terminology.fraunhofer.de/voc/raum#"))
        }
    ```

5. Im Anschluss müssen die hierarchischen Beziehungen erstellt werden:

    ```SPARQL
    PREFIX owl: <http://www.w3.org/2002/07/owl#>
    PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
    PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
    PREFIX skos: <http://www.w3.org/2004/02/skos/core#>
    
    CONSTRUCT { ?s a owl:Class . ?s rdfs:subClassOf ?y . } 
    WHERE { ?s a skos:Concept . ?s skos:broader ?y.
    FILTER (STRSTARTS(str(?s), "https://terminology.fraunhofer.de/voc/raum#"))
    }

    ```

6. Property punning
Folgende properties auch als owl:AnnotationProperty anlegen:
- <http://www.w3.org/2004/02/skos/core#related> 
- <http://www.w3.org/2004/02/skos/core#broadMatch>
- <http://www.w3.org/2004/02/skos/core#relatedMatch>
- http://www.w3.org/2004/02/skos/core#closeMatch
- http://www.w3.org/2004/02/skos/core#exactMatch
- http://www.w3.org/2004/02/skos/core#narrowMatch

7. Direkten Import von <https://purl.org/fidbaudigital/subjects#P49dfc9fd-8edc-46f8-ac84-f58a207cb9fe> entfernen.

8. Speichern.
