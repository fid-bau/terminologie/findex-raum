from rdflib import Graph
from pathlib import Path

FindexRaumowl = Path(__file__).parent.parent.parent/'findex-raum.owl'
FindexRaumttl = Path(__file__).parent.parent.parent/'findex-raum.ttl'
raum4TS = Path(__file__).parent.parent.parent/ 'TS' /'findex-raum.owl'
raum2OpenAlex = Path(__file__).parent.parent.parent/ 'mappings' /'raum2OpenAlex'/ 'raum2OpenAlex+metadata.ttl'
OpenAlex2raumowl = Path(__file__).parent.parent.parent/ 'mappings' /'OpenAlex2raum'/ 'OpenAlex2raum.owl'
OpenAlex2raumttl = Path(__file__).parent.parent.parent/ 'mappings' /'OpenAlex2raum'/ 'OpenAlex2raum.ttl'
TSskos = Path(__file__).parent.parent.parent/ 'TS' / 'findex-raum-skos.owl'
# print (FindexRaumowl)
# print (FindexRaumttl)
# print (raum4TS)
# print (raum2OpenAlex)
# print (OpenAlex2raumowl)
# print (OpenAlex2raumttl)
# print (TSskos)


# def load_file(filepath, file_format):
#     g = rdflib.Graph()
#     g.parse(filepath, format=file_format)
#     return g

def parse_onto(onto_purl, onto_format):
    g = Graph()
    g.parse(onto_purl, format=onto_format)
    print(f"Graph {onto_purl} has {len(g)} statements.")
    assert len(g) > 0, f'Error: No triples found in {onto_purl}.'
    assert g, f'Error: {onto_purl} is not a graph'

parse_onto(str(FindexRaumowl), onto_format="xml")
parse_onto(str(FindexRaumttl), onto_format="turtle")
parse_onto(str(raum4TS), onto_format="xml")
parse_onto(str(raum2OpenAlex), onto_format="turtle")
parse_onto(str(OpenAlex2raumowl), onto_format="xml")
parse_onto(str(OpenAlex2raumttl), onto_format="turtle")
parse_onto(str(TSskos), onto_format="xml")

